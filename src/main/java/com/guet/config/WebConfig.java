package com.guet.config;

import com.guet.interceptor.ProjectInterceptor;
import com.guet.interceptor.RefreshTokenInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // "/**"才表示所有
        registry.addInterceptor(new ProjectInterceptor())
                .excludePathPatterns("/shop/**","/shop-type/**","/voucher/**",
                        "/upload/**","/blog/hot","/user/code","/user/login").order(1);
        // 对所有请求都刷新token
        // order越小,优先级越高
        registry.addInterceptor(new RefreshTokenInterceptor(stringRedisTemplate)).order(0);
    }
}
