package com.guet.controller;


import com.guet.domain.Blog;
import com.guet.domain.dto.UserDTO;
import com.guet.service.BlogService;
import com.guet.utils.Result;
import com.guet.utils.UserHolder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/blog")
public class BlogController {

    @Resource
    private BlogService blogService;

    @PostMapping
    public Result saveBlog(@RequestBody Blog blog) {
        return blogService.save(blog);
    }

    /**
     * 点赞 or 取消点赞
     */
    @PutMapping("/like/{id}")
    public Result likeBlog(@PathVariable Long id) {
        blogService.updateLiked(id);
        return Result.ok();
    }

    @GetMapping("/of/me")
    public Result queryMyBlog(@RequestParam(defaultValue = "1") Integer page) {
        UserDTO user = UserHolder.getUser();
        List<Blog> blogs = blogService.myBlog(user.getId(), page);
        return Result.ok(blogs);
    }

    @GetMapping("/hot")
    public Result queryHotBlog(@RequestParam(defaultValue = "1") Integer current) {
        List<Blog> blogs = blogService.hotBlog(current);
        return Result.ok(blogs);
    }

    @GetMapping("/{blogId}")
    public Result queryBlogId(@PathVariable Long blogId) {
        return blogService.queryBlogById(blogId);
    }

    @GetMapping("/likes/{id}")
    public Result queryBlogLikes(@PathVariable Long id){
        return blogService.queryBlogLikes(id);
    }

    /**
     * 指定用户的文章
     */
    @GetMapping("/of/user")
    public Result queryBlogByUserId(@RequestParam(defaultValue = "1") Integer current,Long id) {
        List<Blog> blogs = blogService.myBlog(id, current);
        return Result.ok(blogs);
    }

    /**
     * 关注的用户的文章
     * @param lastMinTimeStamp 上次结果中的最小时间戳 (最小score)
     * @param offset 上次结果中, score为最小score的元素个数
     */
    @GetMapping("/of/follow")
    public Result queryBlogOfFollow(
            @RequestParam("lastId") Long lastMinTimeStamp,
            @RequestParam(defaultValue = "0") Integer offset){
        return blogService.queryBlogOfFollow(lastMinTimeStamp,offset);
    }
}
