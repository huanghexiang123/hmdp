package com.guet.controller;


import com.guet.domain.Shop;
import com.guet.service.ShopService;
import com.guet.utils.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/shop")
public class ShopController {

    @Resource
    public ShopService shopService;

    /**
     * 根据id查询商铺信息
     */
    @GetMapping("/{id}")
    public Result queryShopById(@PathVariable Long id) {
        return shopService.getById(id);
    }

    /**
     * 新增商铺信息
     */
    @PostMapping
    public Result saveShop(@RequestBody Shop shop) {
        shopService.save(shop);
        return Result.ok(shop.getId());
    }

    /**
     * 更新商铺信息
     */
    @PutMapping
    public Result updateShop(@RequestBody Shop shop) {
        // todo 待完善
        return shopService.updateById(shop);
    }

    /**
     * 根据商铺类型分页查询商铺信息
     */
    @GetMapping("/of/type")
    public Result queryShopByType(
            Integer typeId,
            @RequestParam(value = "current", defaultValue = "1") Integer page,
            Double x,
            Double y
    ) {
        List<Shop> list = shopService.getByType(typeId,page,x,y);
        return Result.ok(list);
    }

    /**
     * 根据关键字分页查询商铺信息
     */
    @GetMapping("/of/name")
    public Result queryShopByName(String name,@RequestParam(defaultValue = "1") Integer page) {
        List<Shop> list = shopService.getByNameLike(name,page);
        return Result.ok(list);
    }
}
