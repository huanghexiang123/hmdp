package com.guet.mapper;


import com.guet.domain.Blog;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface BlogMapper{

    @Insert("insert into tb_blog values(null,#{shopId},#{userId},#{title},#{images}," +
            "#{content},#{liked},#{comments},#{createTime},#{updateTime})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    boolean save(Blog blog);

    @Update("update tb_blog set liked = liked + #{increment} where id = #{id}")
    boolean updateLiked(@Param("increment")int increment, @Param("id") Long id);

    @Select("select * from tb_blog where user_id = #{userId}")
    List<Blog> getByUserId(Long userId);

    @Select("select * from tb_blog")
    List<Blog> getAll();

    @Select("select * from tb_blog where id = #{blogId}")
    Blog getById(Long blogId);

    List<Blog> getByIds(List<Long> blogIds);
}
