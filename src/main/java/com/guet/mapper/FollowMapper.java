package com.guet.mapper;


import com.guet.domain.Follow;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface FollowMapper{

    // user_id 关注了 follow_user_id
    @Select("select count(*) from tb_follow where user_id = #{userId} and follow_user_id = #{id}")
    Integer isFollow(@Param("userId") Long userId, @Param("id") Long id);

    @Insert("insert into tb_follow values(null,#{userId},#{followUserId},#{createTime})")
    boolean save(Follow follow);

    @Delete("delete from tb_follow where user_id = #{userId} and follow_user_id = #{id}")
    boolean delete(@Param("userId") Long userId, @Param("id") Long id);

    @Select("select * from tb_follow where follow_user_id = #{followUserId}")
    List<Follow> getFans(Long followUserId);
}
