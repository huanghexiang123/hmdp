package com.guet.mapper;


import com.guet.domain.SeckillVoucher;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface SeckillVoucherMapper{


    @Insert("insert into tb_seckill_voucher values(#{voucherId},#{stock},#{createTime}," +
            "#{beginTime},#{endTime},#{updateTime})")
    void save(SeckillVoucher skv);

    @Select("select * from tb_seckill_voucher where voucher_id = #{id}")
    SeckillVoucher getById(Long id);

    @Update("update tb_seckill_voucher set stock = stock - #{num} where voucher_id = #{voucherId} and stock > 0")
    boolean updateStock(@Param("num") int num, @Param("voucherId") Long voucherId);
}
