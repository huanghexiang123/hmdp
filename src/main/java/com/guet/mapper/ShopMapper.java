package com.guet.mapper;


import com.guet.domain.Shop;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ShopMapper{

    @Select("select * from tb_shop where id = #{id}")
    Shop getById(Long id);


    @Insert("insert into tb_shop values(null,#{name},#{typeId},#{images},#{area}," +
            "#{address},#{x},#{y},#{avgPrice},#{sold},#{comments},#{score},#{openHours}," +
            "#{createTime},#{updateTime})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void save(Shop shop);

    @Select("select * from tb_shop where type_id = #{typeId}")
    List<Shop> getByType(Integer typeId);

    @Select("select * from tb_shop")
    List<Shop> getAll();

    @Select("select * from tb_shop where name like '%${name}%' ")
    List<Shop> getByNameLike(String name);

    void updateById(Shop shop);

    List<Shop> getByIds(List<Long> ids);
}
