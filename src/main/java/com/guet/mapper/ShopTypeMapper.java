package com.guet.mapper;


import com.guet.domain.ShopType;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ShopTypeMapper{

    @Select("select * from tb_shop_type")
    List<ShopType> getAll();
}
