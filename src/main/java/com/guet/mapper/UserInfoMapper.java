package com.guet.mapper;


import com.guet.domain.UserInfo;
import org.apache.ibatis.annotations.Select;

public interface UserInfoMapper{

    @Select("select * from tb_user_info where user_id = #{userId}")
    UserInfo getById(Long userId);
}
