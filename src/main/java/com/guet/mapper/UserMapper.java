package com.guet.mapper;


import com.guet.domain.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserMapper{

    @Select("select * from tb_user where id = #{userId}")
    User getById(Long userId);

    @Select("select * from tb_user where phone = #{phone}")
    User getByPhone(String phone);

    @Insert("insert into tb_user values(null,#{phone},#{password},#{nickName}," +
            "#{icon},#{createTime},#{updateTime})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    void save(User user);

    List<User> getByIds(List<Long> ids);
}
