package com.guet.mapper;


import com.guet.domain.Voucher;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;

import java.util.List;

public interface VoucherMapper{

    List<Voucher> queryVoucherOfShop(Long shopId);

    @Insert("insert into tb_voucher values(null,#{shopId},#{title},#{subTitle}," +
            "#{rules},#{payValue},#{actualValue},#{type},#{status},#{createTime},#{updateTime})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    void save(Voucher voucher);
}
