package com.guet.mapper;


import com.guet.domain.VoucherOrder;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface VoucherOrderMapper{

    @Insert("insert into tb_voucher_order values(#{id},#{userId},#{voucherId},#{payType}," +
            "#{status},#{createTime},#{payTime},#{useTime},#{refundTime},#{updateTime})")
    void save(VoucherOrder voucherOrder);


    @Select("select count(1) from tb_voucher_order where user_id = #{userId} and voucher_id = #{voucherId}")
    int getByUserIdAndVoucherId(@Param("userId") long userId,@Param("voucherId") long voucherId);
}
