package com.guet.service;


import com.guet.domain.Blog;
import com.guet.utils.Result;

import java.util.List;

public interface BlogService {


    Result save(Blog blog);

    void updateLiked(Long blogId);

    List<Blog> myBlog(Long userId, Integer page);

    List<Blog> hotBlog(Integer page);

    Result queryBlogById(Long blogId);

    Result queryBlogLikes(Long id);

    Result queryBlogOfFollow(Long lastId, Integer offset);
}
