package com.guet.service;


import com.guet.domain.Follow;
import com.guet.utils.Result;

import java.util.List;

public interface FollowService {

    Result follow(Long id, Boolean isFollow);

    Result isFollow(Long id);

    Result followCommons(Long id);

    List<Follow> getFans(Long followUserId);
}
