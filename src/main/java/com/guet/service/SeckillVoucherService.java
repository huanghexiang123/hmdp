package com.guet.service;


import com.guet.domain.SeckillVoucher;

public interface SeckillVoucherService {

    void save(SeckillVoucher skv);

    SeckillVoucher getById(Long id);

    boolean updateStock(int num, Long voucherId);
}
