package com.guet.service;


import com.guet.domain.Shop;
import com.guet.utils.Result;

import java.util.List;

public interface ShopService {

    Result getById(Long id);

    void save(Shop shop);

    List<Shop> getByType(Integer typeId, Integer page, Double x, Double y);

    List<Shop> getByNameLike(String name, Integer page);

    Result updateById(Shop shop);
}
