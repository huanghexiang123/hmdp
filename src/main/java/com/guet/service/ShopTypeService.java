package com.guet.service;


import com.guet.domain.ShopType;

import java.util.List;

public interface ShopTypeService {

    List<ShopType> getAll();
}
