package com.guet.service;


import com.guet.domain.UserInfo;

public interface UserInfoService {

    UserInfo getById(Long userId);
}
