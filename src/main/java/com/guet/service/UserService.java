package com.guet.service;


import com.guet.domain.User;
import com.guet.domain.dto.LoginFormDTO;
import com.guet.utils.Result;

import java.util.List;

public interface UserService {

    User getById(Long userId);

    Result sendCode(String phone);

    Result login(LoginFormDTO loginForm);

    List<User> getByIds(List<Long> ids);

    Result sign();

    Result signCount();
}
