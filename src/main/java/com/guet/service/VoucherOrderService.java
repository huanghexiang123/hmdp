package com.guet.service;


import com.guet.domain.VoucherOrder;
import com.guet.utils.Result;

public interface VoucherOrderService {

    Result seckillVoucher(Long id);

    void createVoucherOrder(VoucherOrder order);
}
