package com.guet.service;


import com.guet.domain.Voucher;
import com.guet.utils.Result;

public interface VoucherService {

    void save(Voucher voucher);

    Result queryVoucherOfShop(Long shopId);

    void addSeckillVoucher(Voucher voucher);
}
