package com.guet.service.impl;

import com.guet.domain.SeckillVoucher;
import com.guet.mapper.SeckillVoucherMapper;
import com.guet.service.SeckillVoucherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class SeckillVoucherServiceImpl implements SeckillVoucherService {

    @Autowired
    private SeckillVoucherMapper seckillVoucherMapper;

    @Override
    public void save(SeckillVoucher skv) {
        seckillVoucherMapper.save(skv);
    }

    @Override
    public SeckillVoucher getById(Long id) {
        SeckillVoucher voucher = seckillVoucherMapper.getById(id);
        return voucher;
    }

    @Override
    public boolean updateStock(int num, Long voucherId) {
        boolean success = seckillVoucherMapper.updateStock(num,voucherId);
        return success;
    }
}
