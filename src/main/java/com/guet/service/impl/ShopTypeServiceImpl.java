package com.guet.service.impl;


import com.github.pagehelper.PageHelper;
import com.guet.domain.ShopType;
import com.guet.mapper.ShopTypeMapper;
import com.guet.service.ShopTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShopTypeServiceImpl implements ShopTypeService {
    @Autowired
    private ShopTypeMapper shopTypeMapper;

    @Override
    public List<ShopType> getAll() {
        PageHelper.orderBy("sort asc");
        List<ShopType> list = shopTypeMapper.getAll();
        return list;
    }
}
