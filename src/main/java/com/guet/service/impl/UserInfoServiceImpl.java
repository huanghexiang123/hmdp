package com.guet.service.impl;


import com.guet.domain.UserInfo;
import com.guet.mapper.UserInfoMapper;
import com.guet.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Override
    public UserInfo getById(Long userId) {
        UserInfo userInfo = userInfoMapper.getById(userId);
        return userInfo;
    }
}
