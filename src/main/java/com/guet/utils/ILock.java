package com.guet.utils;

public interface ILock {
    /**
     * 尝试获取锁
     * @param timeout 锁持有的时间,过期后自动释放
     * @return true表示成功
     */
    boolean tryLock(long timeout);

    
    // 释放锁
    void unlock();
}
