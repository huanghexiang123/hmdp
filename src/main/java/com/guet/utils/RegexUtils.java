package com.guet.utils;

import cn.hutool.core.util.StrUtil;


public class RegexUtils {


    // 手机号
    public static final String PHONE_REGEX = "^1([38][0-9]|4[579]|5[0-3,5-9]|6[6]|7[0135678]|9[89])\\d{8}$";
    // 邮箱
    public static final String EMAIL_REGEX = "^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";
    // 密码正则(4~32位的字母、数字、下划线)
    public static final String PASSWORD_REGEX = "^\\w{4,32}$";
    // 验证码(6位数字或字母)
    public static final String VERIFY_CODE_REGEX = "^[a-zA-Z\\d]{6}$";



    /**
     * 是否是无效手机格式
     * @param phone 要校验的手机号
     * @return true:符合，false：不符合
     */
    public static boolean isPhoneInvalid(String phone){
        return mismatch(phone, PHONE_REGEX);
    }
    /**
     * 是否是无效邮箱格式
     * @param email 要校验的邮箱
     * @return true:符合，false：不符合
     */
    public static boolean isEmailInvalid(String email){
        return mismatch(email, EMAIL_REGEX);
    }

    /**
     * 是否是无效验证码格式
     * @param code 要校验的验证码
     * @return true:符合，false：不符合
     */
    public static boolean isCodeInvalid(String code){
        return mismatch(code, VERIFY_CODE_REGEX);
    }

    // true:不符合
    // false:符合
    private static boolean mismatch(String str, String regex){
        return StrUtil.isBlank(str) || !str.matches(regex);
    }

    public static void main(String[] args) {
        System.out.println(mismatch("   ",PHONE_REGEX));
    }
}
