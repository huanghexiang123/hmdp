package com.guet.utils;


import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.BooleanUtil;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * 简易的 Redis分布式锁
 */
public class SimpleRedisLock implements ILock{

    // 不同业务使用分布式锁时, 采用不同的键名
    private String name;
    private static final String keyPrefix = "Dlock:";
    private static final String idPrefix = UUID.randomUUID().toString(true) + "-";
    private StringRedisTemplate stringRedisTemplate;
    private static final DefaultRedisScript<Long> UNLOCK_SCRIPT;

    static {
        UNLOCK_SCRIPT = new DefaultRedisScript<>();
        UNLOCK_SCRIPT.setLocation(new ClassPathResource("unlock.lua"));
        UNLOCK_SCRIPT.setResultType(Long.class);
    }

    public SimpleRedisLock(String name,StringRedisTemplate stringRedisTemplate){
        this.name = name;
        this.stringRedisTemplate = stringRedisTemplate;
    }

    @Override
    public boolean tryLock(long timeout) {
        // idPrefix区分集群模式下不同实例的线程
        String threadId = idPrefix + Thread.currentThread().getId();
        // 有过期时间,自动释放
        Boolean flag = stringRedisTemplate.opsForValue()
                .setIfAbsent(keyPrefix + name, threadId, timeout, TimeUnit.SECONDS);
        return BooleanUtil.isTrue(flag);
    }

    @Override
    public void unlock() {
        // 调用lua脚本,确保多条redis命令执行时的原子性
        stringRedisTemplate.execute(
                UNLOCK_SCRIPT,
                Arrays.asList(keyPrefix + name),
                idPrefix + Thread.currentThread().getId()
        );
    }
}
