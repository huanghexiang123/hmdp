local voucherId = ARGV[1]; -- 优惠券id
local userId = ARGV[2]; -- 用户id
local orderId = ARGV[3]; -- 订单id

local stockKey = 'seckill:stock:' .. voucherId -- 库存(..是拼接字符串)
local orderKey = 'seckill:order:' .. voucherId -- 订单(set集合)

-- 库存不足
if(tonumber(redis.call('get',stockKey)) <= 0) then
  return 1
end

-- userId已下过单(使用set结构)
if(redis.call('sismember',orderKey,userId) == 1) then
  return 2
end

-- 库存充足且没下过单
-- 扣减库存,下单(添加到set集合中)
redis.call('incrby',stockKey,-1)
redis.call('sadd',orderKey,userId)
-- 发送消息到队列中
redis.call('xadd','stream.orders','*','userId',userId,'voucherId',voucherId,'id',orderId)
return 0