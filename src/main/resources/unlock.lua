-- 比较线程标识与redis中存的是否一致
-- lua脚本中多条redis命令是原子性的
-- 数组索引从1开始
if(redis.call('get',KEYS[1]) == ARGV[1]) then
    -- 释放锁
    return redis.call('del',KEYS[1])
end
return 0